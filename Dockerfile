FROM alpine:latest

ENV USER="kubernetes"
ENV HOME="/home/${USER}"
ENV KUBERNETES_VERSION="v1.14.1"

RUN mkdir -p $HOME && \
  adduser -s /bin/bash -u 1001 -G root -h $HOME -S -D $USER
RUN apk add --update ca-certificates && \
  apk add --update -t deps curl && \
  curl -L https://storage.googleapis.com/kubernetes-release/release/${KUBERNETES_VERSION}/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl && \
  chmod +x /usr/local/bin/kubectl && \
  apk del --purge deps && \
  rm /var/cache/apk/*

USER $USER
WORKDIR $HOME
